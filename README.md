## Cloned package
https://github.com/capacitor-community/intercom
capacitor-community/intercom

## Installation

Using npm:

```bash
npm install https://bitbucket.org/ctrltechnologies/intercom-multi-workspace.git
```

Sync native files:

```bash
npx cap sync
```

## API

- loadWithKeys
- registerIdentifiedUser
- registerUnidentifiedUser
- updateUser
- logout
- logEvent
- displayMessenger
- displayMessageComposer
- displayHelpCenter
- displayLauncher
- hideLauncher
- displayInAppMessages
- hideInAppMessages
- setUserHash
- setBottomPadding

## Usage

```js

import { Plugins } from "@capacitor/core";
const { PushNotifications } = Plugins;

//
// Register for push notifications from Intercom
PushNotifications.register()

//
// Initialize plugin with config values
Plugins.IntercomMultiWorkspacePlugin.loadWithKeys({appId: "yyy", apiKeyIOS: "ios_sdk-xxx", apiKeyAndroid: "android_sdk-xxx"})

//
// Register an indetified user
Plugins.IntercomMultiWorkspacePlugin.registerIdentifiedUser({ userId: 123456 }) // or email or both

//
// Register a log event
Plugins.IntercomMultiWorkspacePlugin.logEvent({ name: "my-event", data: { pi: 3.14 } })

//
// Display the message composer
Plugins.IntercomMultiWorkspacePlugin.displayMessageComposer({ message: "Hello there!" } })

```

## Android setup

In android case we need to tell Capacitor to initialise the plugin:

> on your `MainActivity.java` file add `import com.getcapacitor.ctrl.intercom.IntercomMultiWorkspacePlugin;` and then inside the init callback `add(IntercomMultiWorkspacePlugin.class);`

Now you should be set to go. Try to run your client using `ionic cap run android --livereload`.

> Tip: every time you change a native code you may need to clean up the cache (Build > Clean Project | Build > Rebuild Project) and then run the app again.