import { Plugins } from '@capacitor/core';
import { IntercomProtocol, UserUpdateOptions } from './definitions';

const { IntercomMultiWorkspacePlugin } = Plugins;

export class Intercom implements IntercomProtocol {

  loadWithKeys(options: {
    appId?: string;
    apiKeyIOS?: string;
    apiKeyAndroid?: string;
  }): Promise<void> {
    return IntercomMultiWorkspacePlugin.loadWithKeys(options);
  }

  registerIdentifiedUser(options: {
    userId?: string;
    email?: string;
  }): Promise<void> {
    return IntercomMultiWorkspacePlugin.registerIdentifiedUser(options);
  }

  registerUnidentifiedUser(): Promise<void> {
    return IntercomMultiWorkspacePlugin.registerUnidentifiedUser();
  }

  updateUser(options: UserUpdateOptions): Promise<void> {
    return IntercomMultiWorkspacePlugin.updateUser(options);
  }

  logout(): Promise<void> {
    return IntercomMultiWorkspacePlugin.logout();
  }

  logEvent(options: { name: string; data?: any }): Promise<void> {
    return IntercomMultiWorkspacePlugin.logEvent(options);
  }

  displayMessenger(): Promise<void> {
    return IntercomMultiWorkspacePlugin.displayMessenger();
  }

  displayMessageComposer(options: { message: string }): Promise<void> {
    return IntercomMultiWorkspacePlugin.displayMessageComposer(options);
  }

  displayHelpCenter(): Promise<void> {
    return IntercomMultiWorkspacePlugin.displayHelpCenter();
  }

  displayLauncher(): Promise<void> {
    return IntercomMultiWorkspacePlugin.displayLauncher();
  }

  hideLauncher(): Promise<void> {
    return IntercomMultiWorkspacePlugin.hideLauncher();
  }

  displayInAppMessages(): Promise<void> {
    return IntercomMultiWorkspacePlugin.displayInAppMessages();
  }

  hideInAppMessages(): Promise<void> {
    return IntercomMultiWorkspacePlugin.hideInAppMessages();
  }

  setUserHash(options: { hmac: string }): Promise<void> {
    return IntercomMultiWorkspacePlugin.setUserHash(options);
  }

  setBottomPadding(options: { value: string }): Promise<void> {
    return IntercomMultiWorkspacePlugin.setBottomPadding(options);
  }
}
